<?php

namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller{
    public function index(){
     $data = ['title' => 'hi i am home'];
      return  $this->render('main/home.html.twig', $data) ; 
    }
}