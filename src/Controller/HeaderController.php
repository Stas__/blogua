<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Page;
class HeaderController extends Controller
{
    
    public function index($title)
    {
        $em = $this-> getDoctrine()->getManager();
        $links = $em->getRepository(Page::class)->getLinkPages();
        return $this->render('main/header.html.twig', ['links'=> $links, 'title'=>$title ]);
            
    }
}
