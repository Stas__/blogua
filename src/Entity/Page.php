<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
   /**
     * @ORM\Table(name="page")
     * @ORM\Entity(repositoryClass="App\Repository\PageRepository")
    
     */




class Page
{/**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;
    function getName(){
        return $this->name;
    }
    function getDescription(){
       return  $this->description;
    }
    function setName($name){
        $this->name = $name;
    }
    function setDescription($description){
        $this->description = $description;
    }
}
