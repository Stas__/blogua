<?php

namespace App\Repository;
use App\Entity\Page;
class PageRepository extends \Doctrine\ORM\EntityRepository {
public function getPage($id){
    $em = $this ->getEntityManager();
     $result = $em->find(Page::class, $id);
    return $result;
}
public function getLinkPages(){
    $em = $this ->getEntityManager();
     $query = $em ->createQuery('SELECT p.id, p.name FROM App:Page p ORDER BY p.id ASC ');
     $result = $query->getResult();
 return $result;
}
}